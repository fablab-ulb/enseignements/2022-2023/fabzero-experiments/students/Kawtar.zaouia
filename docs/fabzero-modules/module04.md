# 4. LaserCut
Nous devons sélectionner un outil du fablab. Il y avait la formation LaserCut, microcontrôleur,CNC et 3D for Repair.
J'ai choisi la formation du Lasercut qui m'interresait beaucoup.

## 4.1 LaserCut
Le LaserCut est une machine qui possède un faisceau laser pour couper ou graver du bois, du métal et d'autres matériaux. Le laser est contrôlé par un ordinateur et dirigé le long d'un chemin prédéterminé pour créer des coupes ,des formes complexes et précises.

Les découpeurs laser émettent un faisceau de lumière très puissant qui est focalisé par une lentille. Le faisceau peut être réglé par rapport à la vitesse d'intensité, à la puissance, ce qui lui permet de réaliser des coupes d'épaisseur et de duretés différentes.

## 4.2 Inkscape

Inkscape est un logiciel qui permet de créer et de modifier des graphiques sous forme vectoriels c'est-à-dire que les images sont consituées de formes et de lignes au contraire des images matricielles qui se composent de pixels.
Inkscape est intérressant pour créer des logos, des textes écrits , des motif etc...

Pour télécharger Inkscape, voici le [lien](https://inkscape.org/release/inkscape-1.2.2/).


## 4.3 Conception en 2D

Notre but ici est de créer une boite à l'aide du cut laser.
Tout d'abord on va se rendre sur [Boxes.py](https://www.festi.info/boxes.py/).
C'est un programme qui est utilisé pour générer des boîtes en 2D ou en 3D. 

Premièrement, j'ai sélectionné "a simple box" et choisi les dimensions de ma boite. Ensuite, j'ai télechargé le fichier sous forme .svg.
![](images/Capture%20d%E2%80%99%C3%A9cran%202023-04-07%20%C3%A0%2014.59.54.png)

Je me dirige vers Inskape et j'ouvre mon fichier. 
Pour ma part,j'ai rajouté un cercle. 
![](images/inskape.png)

**Problèmes**: Je devais brancher mon ordinateur à la machine avec un câble USB. Ayant un mac, je n'ai pas d'USB sur mon ordinateur et je n'ai pas d'adaptateur avec moi. Le professeur m'a gentiment prêté son ordinateur pour que je réalise mon objet. 

Plusieurs paramètres rentrent en jeu, comme évoqué précédemment, la vitesse d'intensité, la puissance et d'autres encore. C'est le professeur qui nous a réglés la vitesse et la puissance . Lors de la création de l'objet, les faces du cube n'étaient pas bien découpées à cause de la puissance qui était faible. J'ai augmenté la puissance et le découpage a était fait.


Après assemblage du puzzle , j'ai obtenu ma petite boite.


![](images/boite.jpg)