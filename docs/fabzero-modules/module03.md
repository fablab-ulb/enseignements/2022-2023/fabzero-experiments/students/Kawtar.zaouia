# 3. Impression 3D


Le  but de cette séance est d'imprimer des objets en 3D.  Notre but est de savoir  utiliser et comprendre le fonctionnement une imprimante en 3D.


## 3.1 Imprimante En 3D
Une imprimante en 3D a pour but d'imprimer des objets en 3D. Cependant, la plupart possèdent des limites. En effet, la taille de l'objet est limitée. Les imprimantes 3D qui se trouvent au Fablab sont incapables d'imprimer des objets de grandes dimensions. De plus, le temps d'impression est généralement long. 

**Structure**

![](images/sch%C3%A9ma.png)
_lien de l'image_: https://www.playhooky.fr/focus/imprimante-3d/ 
![](images/341017708_607213814790731_1460264277441599383_n.jpg)

- Plateau : surface sur laquelle l'objet est imprimé
- Moteur: moteur contrôle le mouvement de la tête d'impression 
- carte électronique : une carte SD est insérée sur la carte électronique qui est le "cerveau" de l'imprimante en 3D.

## 3.2 PrusaSclicer 
PrusaSlicer est un logiciel de découpage d'impression 3D gratuit. Il permet de prendre un modèle 3D pour l'impression tout en convertissant des couches 2D qui seront imprimées une par une.
PrusaSlicer est compatible avec une gamme d'imprimantes 3D et est largement utilisé dans la communauté de l'impression 3D. 


### **Comment fonctionne PrusaSlicer?**
Tout d'abord, il faut télécharger et installer Prusaclicer, en appuyant sur ce [lien](https://www.prusa3d.com/fr/page/prusaslicer_424/).

Ensuite il faut importer un ficher. Pour ma part, c'était un fichier d'OpenScad sous forme .stl. Bien sûr, vous pouvez prendre d'autres fichiers sous une autre forme. 

## 3.3 Modélisation d'une pièce
La pièce que je vais imprimer est une flexlinks réalisé grâce à OpenScad.
![](images/flexlinks.png)

La pièce va être décomposée en plusieurs parties.

Premièrement, je vais définir les paramètres :

```
$fn=50;
hauteur=4.7;
distance_de_separation=8;
bord=1.7;
longueur=40;
epaisseur=1.4;
n=2; // nombre de trou
rayon=4;
```

### tête 
Création de la tête, elle peut être visualisée comme une fusion de deux cylindres côte à cote. Pour réaliser cette fusion, je vais utiliser la commande ```hull```. Pour créer les trous, je vais soutraire  deux cylindres à l'intérieur de la tête avec la commande ```difference```.

```
module tete(){
    difference(){
        hull(){
            cylinder(h = hauteur, r = rayon+bord,     center = True);
            translate([(n-1)*distance_de_separation,0,0]){cylinder(h =  hauteur, r = rayon+      bord,      center = True);}
         }
            cylinder(h = hauteur, r = rayon, center = True);
            translate([(n-1)*distance_de_separation,0,0]){cylinder(h = hauteur, r = rayon, center = True);}
  }  
}  
tete();
```
![](images/tete.png)

Pour créer la deuxième tête, je vais la translater à la distance qui les séparent avec la commande ```translate```.

```
translate([(-distance_de_separation)-(2*rayon)-(2*bord)-longueur,0,0]) 
    tete();
```

![](images/Cflexlinks%202%20tete..png)

### Manche qui relie les 2 têtes

Pour réaliser le manche, je crée un cube avec les dimensions définies dans les paramètres. Le cube créé sera au centre. Je dois la translater pour qu'il soit entre les deux têtes.
```
translate ([-longueur-rayon-bord,(-epaisseur/2),0])
cube([longueur,epaisseur ,hauteur], center=True); 
```
![](images/finale.png)


Tous les codes que j'ai utilisé sont sous licence. Licence : Creative Commons Attribution 4.0 International (CC BY 4.0).
```
/*
Fichier : Flexlinkes.scad

Auteur : Zaouia Kawtar 

Date : 14/03/22

Licence : Creative Commons Attribution 4.0 International (CC BY 4.0)

//*/

```


## 3.4 Impression

Pour imprimer mon objet, je dois ouvrir  Prusaclicer et ouvrir mon fichier.


L'objet est présenté sur un plateau. Pour ma part, j'ai mis une bordure autour de mon objet. Vu la simplicité de mon objet, je n'ai pas dû paramétrer. Le temps d'impression se trouve en bas à droite. Mon objet présente un temps de 8 minutes qui est tout à fait réalisable.
![](images/Pruscaclicer.bordure.png)

Pendant l'impression, l'impression de mon objet à été un échec, car la bordure était trop épaisse pour un objet petit. J'ai dû changer l'épaisseur de la bordure. Je l'ai réduit à 2,5 mm.
![](images/Changement%20bordure.png)

Pour plus d'infos, j'ai trouvé une fiche explicative sur les paramètres de PrusaSclicer et la manière de les employer. En appuyant sur ce [lien]([lien](https://www.prusa3d.com/fr/page/prusaslicer_424/).).
Vous retrouverez comment utiliser Prusaclicer.


