# 1. Documentation
Ce premier module aborde la documentation et la démarche de la création de mon site. Les obstacles que j'ai pu rencontrer et comment je les ai résolu.

## Documentation, c'est quoi?

La documentation d'un projet, c'est la procédure par laquelle le projet a pu être réalisé(la démarche, les problèmes et leurs solutions). 

## Pourquoi faire une documentation ?

La documentation permet à quiconque qui découvre votre projet de savoir de quoi il s'agit comment il a été conçu. 
De plus, la documentation est un excellent outil pour soi-même afin de garder une trace de la réalisation du projet.


## 1. Git 
Git est un système de contrôle. Il a été crée en 2005 par Linus Torvalds. De plus en plus de projets logiciels se basent sur Git par exemple le contrôle de version, y compris des projets commerciaux et en open source.

Pour éviter de se connecter chaque fois à Gitlab, il est nécessaire de créer une connexion entre le serveur Gitlab et celui de notre ordinateur afin de travailler à distance. 

### 1.1 configuration git
Tout d'abord, je vais paramétrer git.  
J'ouvre mon terminal qui est déjà installé sur MacOS. 

Pour paramétrer, j'inscris mon nom utilisateur et mon adresse mail qui correspondent à ceux de Gitlab, la commande `git config` est utile pour les configurer. 
La commande `--global`  permet d'avoir une configuration global et non spécifique. 
![](images/git_config.png)



### 1.2 Clé SSH
La clé SSH est une clé cryptographique utilisée afin d'identifier  un utilisateur et lui permettre d'accéder à un serveur via le protocole SSH.
La paire de clé se décompose en 2 clés:
- clé publique
- clé privée 

**Génerer la clé SSH**
 
 Pour génerer une paire de clé SSH, on va utiliser la commande suivante. Pour mon cas, j'ai utilisé la clé ED25519.
 `ssh-keygen -t ed25519* -C  "your_email@example.com"` 

![](images/ssh.png)

Une clé privée et publique sera créée.
Ensuite, il faut taper simplement sur enter, c'est ainsi que la clé sera stocké dans un fichier par défaut dans l'ordinateur. 
Il vous sera demandé d'entrer un paraphrase qui s'agit d'un mot de passe.

![](images/paraphrase.png)

Après avoir créé la clé SSH, on doit créer une copie de la clé publique qui sera sur notre Gitlab. Le code est le suivant:
`tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy`

Sur gitlab, il faut aller dans SSH keys et coller la clé publique.
La clé publique a été enregistrée dans un fichier nommé id/225.19.
Il faut préciser que sur MacOS ce fichier est caché. Pour le retrouver, il faut aller dans le disque dur de l'ordinateur et aller chez  utilisateur. 
Appuyez sur les touches  [cmd] + [shift] + [.]. 
Tous les fichiers cachés apparaîtront, un dossier nommé .SSH apparaîtra dans celui-ci et le fichier id/225.19. 
Copier la clé publique et coller sur Gitlab.

**Cloner les dossier** 

Pour cloner les dossiers, il suffit de copier le lien lié à la clé SSH. Ce lien se trouve sur votre Gitlab. Il suffit d'aller après  dans le terminal et de taper la commande ` git clone +lien`.

A présent, vous pouvez faire des modifications sur votre site et dans les dossiers.

**Commande**

|commande| manipulation|
|:-------| :-----------|
|git pull| Télécharge la dernière version
|git add *| Ajoute les modifications dans votre fichier dans lequel vous êtes.
|git commit -m "commentaire"| écris un commentaire dans vos modifications
| git push| envoye les changements vers le serveur


## 2. Editeur de texte 
Un éditeur de texte nous permet d'éditer ou de créer des textes. Pour ma part, j'utilise _Visual Studio Code_ que j'ai téléchargé via ce lien:[ Code visual studio](https://code.visualstudio.com ). 


 Les fichiers présents sont écrits en _Markdown_. Voici un [site](https://docs.framasoft.org/fr/grav/markdown.html) que j'ai utilisé qui reprend toute la synthaxe de _Markdown_.

###2.1 fichier mkdocs.yml

Le fichier mkdocs.yml doit être mise à jour. Name, site_url doivent correspondre au lien du site, le repor_url celui de Gitlab et le thème.
![](images/macdoyl.png)

**Important**: Pendant, toute cette documentation [Dao Kodjo](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/kodjo.dao/-/blob/main/docs/fabzero-modules/module01.md) m'a aidé pour effectuer ces tâches. Je vous conseil de lire sa documentation si vous avez rencontré des problèmes. 

### 2.2 Mkdocs
Ensuite j'ai installé python via ce [lien](https://www.python.org/downloads/).

Dans le terminal, j'ai écrit `pip --version` et `python --version`.
En effet, la commande pip et python n'est pas reconnue.  

![](images/pip.png)

Cela est du au faites que j'ai installé la version du python 3.11 et que la commande pip et python ne fonctionnent que pour les versions antérieures. On doit alors utiliser la commande `pip3`  et `python3`.

Dans le termianl, je tape `pip3 --version`

Ensuite `pip3 install mcdocks`
`mkdocs --version`
`mkdocs serve`
![](images/mkdocs.png)
la commande `mkdocsserve`  permet de voir un aperçu de votre site avant d'effectuer la mise à jour.


## 3.Image 
GraphicsMagick est un logiciel capable de modifier et d'éditer des  images. GraphicsMagick peut éditer des images matricielles et vectorielles.

Dans le terminal, j'installe Homebrew.

`
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
`

Lorsque l'installation est finie ,j'installe GraphicsMagick avec le code suivant:
`brew install graphicsmagick`

Pour voir si GraphicsMagick a été installé, j'écris:
`gm version`

**Remarque** : L'installation Homebrew et De GraphicsMagick peuvent prendre quelques secondes. Si vous voyez que  rien ne s'affiche dans votre terminal après avoir écrit le code. Pas d'inquiétude, il suffit juste de patienter quelques secondes parfois quelques minutes.

###3.1 Manipulations
Voici quelques instructions utiles pour manipuler GraphicsMagick.



| manipulation | code |
|:-------------|-----:|
|redimensionner|`gm convert input.jpg -resize "800x600" ouput.jpg`|
|recadrer| `gm convert input.jpg -crop 400+300+100.50 output.jpg`|
|convertir le format|` gm convert input.jpg output.png`|

- Redimensionner une image : changer la longueur et la hauteur de l'image
- Recadrer à une position ou une taille spécifique
- Convertir une image.jpg en .png ou inversemment.

_input.jpg /.png réprésente le nom de votre fichier._
_output.jpg /.png représente le nom que vous désirez après les modifications._

J'ai redimensionné une image de fleur. J'ai réduit sa longueur et hauteur.
Dans mon terminal, j'écris :

![](images/fkeur.png)
Je rentre dans mon dossier dans lequel se trouve mon image à l'aide de la commande 
`cd`. Ensuite, j'insère le code et les dimensions que je souhaite.

**Remarque**: bien inscrire les " " lorsque vous avez des espaces dans le nom de vos fichiers. Exemple " fleur converti.jpg" et non fleur converti.jpg
![](images/fleur.jpg)
![](images/fleur%20converti.jpg)

