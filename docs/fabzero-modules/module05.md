# 5. Dynamique de groupe et projet final
Ce module aborde la formation des groupes, les outils éethodologiques pour la réalisation du projet, la thématique du projet et la problématique .


## 5.1 Analyse et conception de projet
Quelques semaines auparavant, nous devions nous inspirer des projets Fablab et Frugal sciences pour faire notre premier arbre à problèmes et solutions au sujet d'une problèmatique présentée.
Ce travail peut se faire par paire (en duo) ou individuellement. J'ai choisi de travailler avec [Mariam](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/mariam.mekray/-/blob/main/docs/index.md) sur le Foldscope.


_Définition_ : _Le Foldscope est un petit microscope en papier d'où vient son nom "folding-paper microscope". Ce microscope est en effet en papier pliable. Son coùt de production est minime; moins de deux dollars, il est donc premièrement destiné aux pays du tiers-monde. Il est d'une réelle efficacité car il permet un grossissement jusqu'à même 2000 fois._ 

Ce microscope est une réelle révolution car plus tard ,il servira à dépister des maladies, ce qui est très utile en période d'épidémie car un vrai microscope est un outil plus cher, moins accessible et très peu maniable surtout sur le terrain.
Ce petit microscope peut même être jetable et donc empêche toute contamination comme pour des pays où les produits de décontamination par milliers sont indisponibles et où les microscopes seraient alors agents véhiculateurs de maladies ou de microbes si pas désinfectés correctement.

Ce microscope est en réalité fait d'un petit papier, avec des composants optiques imprimés en 3D, un éclairage LED, une lentille. Il faut y insérer une lame ou une lamelle pour fonctionner.
Pour ce premier cours, on devait chacun ou en groupe réaliser un arbre à problème et un arbre à objectif.

 ### Arbre à problèmes: 
 Outil méthodologique schématique pour mieux identifier un problème avec ses causes et ses conséquences.

**Structure**
  - Le tronc représente le problème central, la problématique
  - Les racines répresentent les causes. Il y a les racines principales qui représentent les causes principales et les ramifications les causes secondaires.
  - les branches représentent les conséquences. On retrouve les branches principales et les ramifications.

![](images/arbe%20a%20roblemeeeeeee.png)

### Arbre à objectif
Outil méthodologique schématique afin de comprendre les objectifs et les liens entre plusieurs niveaux objectifs.
3 niveaux sont réprésentés.
1. Objectif principal
2. Objectifs intermédiaires
3. Objectifs opérationnels

![](images/Cfini.png)








# 5.2 formation du groupe 

Chacun de nous devait présenter un objet. Pour ma part, j'ai choisi une lingette démaquillante dans laquelle je me suis démaquillée. En ayant rien sur mon visage, j'ai remarqué que celle-ci présentait des traces brunes. Ces traces représentent la pollution contenue dans l'air. En effet, chaque jour des milliers de particules fines se déposent sur notre visage. Je voulais démontrer à travers de cet objet, la qualité de l'air et la pollution. La présentation de nos objets, nous permet de faire des liens entre ceux présents dans la classe et d'unir nos objets qui présentes des similitudes et de former un groupe. Malheureusement, j'ai présenté mon objet un peu tard et la création des groupes était déjà faites. Ce que j'ai fait, j'ai parcouru les groupes et demander les thèmes de chacun. Des groupes abordaient la pollution; mais ils étaient déjà complets.

 Après réflexion , j'ai décidé de rejoindre un groupe composé de 4 membres qui abordait le recyclage et précisémment "le recyclage des plastiques".
Pour plus d'informations concernant la formation de ce groupe et la thématique, vous pouvez aller lire la documentation de [Alexandre Hallemans ](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/alexandre.hallemans/-/blob/main/docs/fabzero-modules/module05.md),[Christophe Ory](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/alexandre.hallemans/-/blob/main/docs/fabzero-modules/module05.md),[Donovan Crousse ](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/alexandre.hallemans/-/blob/main/docs/fabzero-modules/module05.md),[Louis Devroye](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/alexandre.hallemans/-/blob/main/docs/fabzero-modules/module05.md)


## 5.3 Prise de décision 


Cette séance avait pour but de nous enseigner les prises de décision au sein d'un groupe. 

l'art de décider: peut être réprésenter sous forme triangulaire dans laquelle chaque sommet représente une manière de prendre une décision. Bien sur, il existe d'autres formes de décison. Mais nous aborderons que 3.
Imaginons les 3 cas extremes:
![](images/triangle.png)
1. groupe qui décide seul: Une décision est prise par une personnes. Il n'y a aucune décision pris ensemble. Tout est controlé par une personne. Cette situation 
2. groupe qui décide ensemble : personne ne vas prendre des décisions seules. Toutes les décisions doivent se faire ensemble même des décisions futiles. Le groupe aura tendance a perdre du temps à vouloir toujours décider ensemble.
3. Groupe ne décide pas: le groupe ne possède pas assez d'information pour prendre une décision. Il aura tendance à perdre du temps et de tourner en rond.


La meilleur chose à faire est de se positioner à l'équilibre dans le triangle.


### méthode de décision

- Majorité: le choix qui a le plus de vote sera la décision finale.
- consensus: chacun du groupe devra accepter le choix final.
- Check: chacun du groupe remue ses mains, vers le haut si on est d'accord, vers le milieu si on n'est pas trop enthousiaste et vers le bas si cette décision ne nous convient pas.

Notre groupe : la prise de décision est le vote majoritaire.


##5.4 choix de la thématique

Tout d'abord, il nous ait demandé de définir une thématique en lien avec nos objets. Les thèmes ressortis sont le recyclage, la pollution et les animaux. Après concertation, on a décidé d'avoir comme thématique: Les déchets rejetés dans l’océan, la bioaccumulation chez les animaux marins.
La problématique  peut être définie comme " L'accumulation du plastique dans le corps des animaux marins qui se retrouvent dans la chaîne trophique". 

Ensuite, on devait définir une contrainte forte. Notre contrainte était la localisation. Nous avons choisi la Meuse.

Par la suite, nous avons créé un arbre à problèmes reprenant les causes et les conséquences de notre problématique.
![](images/arbre%20reclage.jpg)