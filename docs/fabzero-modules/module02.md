# 2. Conception Assistée par Ordinateur et les mécanimes flexibles.
le but de cette séance 


## 2.1 Les mécanismes compliants, flexibles et flexions


**mécanisme compliant**
est un mécanisme flexible qui transmet la force et le mouvement par la déformation élastique du corps. 

**flexions**  
mouvements d'un corps traîne à une diminution de l'angle entre deux segments. 


## 2.2 logiciel 3D et 2D

La plupart des imprimantes 3D sont des outils qui servent à fabriquer des pièces de petites tailles. D'autres imprimantes sont capables d'imprimer des objets de plus grandes dimensions. 
Cependant, les objets doivent être modélisés grâce à des logiciels de modélisations 3D. 

Un logiciel fonctionne principalement sur la manipulation de forme. Par exemple, des cubes, des sphères ou des cônes.

Les logiciels qui vont être utilisés sont Freecad et OpenScad. Ces logiciels sont intérresants car ils se basent sur des modélisations paramétriques. La conception paramétrique est un mode de fonctionnement de logiciels de conception assistés par des ordinateurs actuels. Il s'agit de définir une entité par des paramètres qui peuvent être modifiés facilement.

### 2.2.2 Freecad

Pour concevoir des objets en 3D, j'ai installé Freecad en appuyant sur ce [lien](https://www.freecad.org/downloads.php).
En étant débutante dans ce domaine là, je vous conseille si vous êtes dans le même cas que moi de regarder ces [vidéos](https://www.youtube.com/watch?v=SPFoXfEAufw) qui expliquent comment utiliser Freecad (elles sont en français !). 

Pour ma part, je vais essayer de créer un coeur le plus simple possible.

Pour faire cela je vais procéder en étape.
1) Dans la partie "Part Design",je choisi un plan en 2D le plan XY. 
2) Je décompose mon coeur en puzzle, c'est-à-dire que j'ajoute 2 segments de droite et 2 arcs de cercle. 
Je remarque une note très important "18" qui veut dire que j'ai 18 degrés de liberté. Le but est de tous les éliminer est d'arriver à 0 pour que la transformation en 3D soit faite.
3) Pour éliminer les degrés de liberté. Je dois utiliser les contraintes. Voici la liste des contraites que j'ai utilisé.
4) Ensuiten je passe ma forme en 2D en 3D.

![début ](images/Capture%20d%E2%80%99%C3%A9cran%202023-03-12%20%C3%A0%2000.07.15.png)
![contraites utilisées](images/Capture%20d%E2%80%99%C3%A9cran%202023-03-12%20%C3%A0%2022.42.47.png)
![coeur](images/coeur%20Freecad.png)

**Remarque**: pour passer de la forme 2D en 3D. Ce message doit être affiché qui veut dire que tous les degrés de liberté ont été éliminés.
![](images/contraire%20ok.png)


### 2.2.3 Openscad 
Openscad est un logiciel qui permet de créer des modèles 3D. Ce qu'il le distingue de Freecad, c'est qu'il utilise un langage de programmation pour déterminer des objets ou des formes 3D.

Pour le télécharger voici le [lien](https://openscad.org) que j'ai utilisé. 
De même que pour Freecad si vous êtes débutant, je vous conseille de regarder ces [tutos](https://www.youtube.com/watch?v=n0HMJ_OfzYE).

Pour ma part, j'ai crée des figures simples.
![](images/openscad%20boule.png)
```sphere(d=20);```

![](images/openscad%202.png)
![](images/openscad.png)

```
difference(){
    cube([40,40,40], center=true);
    sphere(25);
}
```

Voici un [site](https://openscad.org/cheatsheet/) avec toutes les commandes utiles pour maitriser OpenScad. 



## Licence Creative Commons

La Licence Creative Commons permet aux autres personnes de pouvoir utiliser nos fichiers. Il existe différentes licences. Si cela vous interrese de savoir l'utilité de chaque licence, si vous voulez en savoir plus. Je vous conseille d'aller sur ce [site](https://creativecommons.org/licenses/?lang=fr)  pour tout renseignement supplémentaire.

Voici le [fichier ](files/freecadcoeur.FCStd)Freecad sur le coeur.
Ce document est sous licence CC-by-sa 4.0. 



