

## A propos de moi :)

![](images/Capture%20d%E2%80%99%C3%A9cran%202023-04-15%20%C3%A0%2019.26.13.png)

Coucou, moi c'est Kawtar. Je suis étudiante en BA3 en bioingénieur. J'ai choisi cette option car le monde de la biologie me fascine que ça soit d'un point vue microscopique et mascroscopique. Etre ingénieur selon moi, signifie de résoudre des challenges grâce au savoir et à la pratique. 

Comme vous voyez sur la photo, j'adore voyager et découvrir de nouvelles cultures. Des belles plages de Tanzanie aux temples maya du Mexique en passant par les canaux de Venise. Voyager me permet de faire connaissance et de rencontrer des personnes avec un mode de vie totalement différent et d'en apprendre davantage sur leur culture et leur mode de vie.


## My background

Durant mon parcours scolaire, j'ai participé à l'exposition des Sciences naturelles à ULB sur le thème de la diversité. Moi et mon groupe, avons décidé de parler de la diversité dans la Grande barrière de Corail.

Voici une photo de notre maquette qui représente la Grande Barrière de Corail.
![](images/IMG_5113-preview.JPG)








